package service;

import model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import product.ProductData;

@Service
public class ProductService {
    private final ProductData productClass;

    @Autowired
    public ProductService(@Qualifier("FakeStu") ProductData studentClass) {
        this.productClass = studentClass;
    }


    public int addProduct(Product product){
        return productClass.insertProduct(product);
    }
}
