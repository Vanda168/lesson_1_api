package product;

import model.Product;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository("FakePro")
public class ProductDataAccess implements ProductData {
    private static List<Product> DB = new ArrayList<>();
    @Override
    public int insertProduct(UUID id, Product product) {
        DB.add(new Product(id,product.getName()));
        return 0;
    }
}
