package product;

import model.Product;

import java.util.UUID;

public interface ProductData {
    int insertProduct(UUID id, Product product);
    default int insertProduct(Product product){
        UUID id = UUID.randomUUID();
        return insertProduct(id,product);
    }
}
