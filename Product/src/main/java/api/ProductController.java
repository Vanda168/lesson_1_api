package api;

import model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.ProductService;

@RequestMapping("api/v1/product")
@RestController
public class ProductController {
    private final ProductService productservice;
    @Autowired
    public ProductController(ProductService productservice) {
        this.productservice = productservice;
    }
    @PostMapping
    public void addStudent(@RequestBody Product product){
        productservice.addProduct(product);
    }
}
